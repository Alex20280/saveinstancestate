package layouts.sourceit.com.comsaveinstancestate;

import android.content.Intent;
import android.os.PersistableBundle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Patterns;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.textViewSavedText)
    TextView textViewSavedText;
    @BindView(R.id.editText)
    EditText editText;
    private Unbinder unbinder;

    public static final String KEY_TEXT = "key";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        if (editText == null) {
            Toast.makeText(MainActivity.this, "no data", Toast.LENGTH_SHORT).show();
        } else {
            Toast.makeText(MainActivity.this, "Data saved: " + KEY_TEXT, Toast.LENGTH_SHORT).show();
            textViewSavedText.setText(savedInstanceState.getString(KEY_TEXT));

        }

    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putString(KEY_TEXT, editText.getText().toString());

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unbinder.unbind();
    }
}

